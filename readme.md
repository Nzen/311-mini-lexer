__311 Mini Lexer__

Takes key file and program files. After storing the keys, outputs program files with identifier flags: keyword* , var_first_time? , var_later@ . These are separated by a single space or a new line.

Accomplishes this via a structure like a flat trie. Has a first char leaf, and then a string of the remaining characters, plus a sparse list of indicies to also check, trie style.

For CPP CS 311, winter 2013.

Compliant. Run with java 1.6+

* javac Project2.java
* java Project2 InputFile1.txt InputFile2.txt [> out.file]

**License**

You can copy/use this code at your own risk, provided it isn't for a school project. In that case, you can only view it as reference material.