
// black box version
public class TrieTester
{
	static final boolean initial_assumption = true;
	static final boolean didnt = false;

	public boolean state_matches( Storage delta, T_Flag should_be )
	{
		return delta.t_matches_state( should_be );
	}

	public boolean save_keys( Storage delta, String[] reserved, char k_f )
	{
		boolean worked = initial_assumption;
		T_Flag shoul_b_diff;
		for ( String nn : reserved )
		{
			IterableString keys = new IterableString( nn );
			for ( Character ch : keys )
			{
				delta.determine_char( ch );
				if ( ! state_matches( delta, T_Flag.saving ) )
				{
					System.out.println( "sk`: saving key not in save mode" );
					worked = didnt;
				}
			}
			shoul_b_diff = delta.check_flag( k_f );
			if ( shoul_b_diff != T_Flag.f_key )
			{
				System.out.println( "sk`: saving key didn't give diff flag" );
				worked = didnt;
			}
			else if ( ! state_matches( delta, T_Flag.initial ) )
			{
				System.out.println( "sk`: ending key didn't change to init state" );
				worked = didnt;
			}
		}
		return worked; // returning at first problem hid later problems
	}

	public boolean save_first_var( Storage delta, String var, char n_f )
	{
		T_Flag said;
		IterableString iable = new IterableString( var );
		for ( Character ind : iable )
		{
			said = delta.determine_char( ind );
			if ( said != T_Flag.f_unseen )
			{
				System.out.println( "sfv: all new var didn't give all T_F unseen" );
				return didnt;
			}
		}
		said = delta.check_flag( n_f );
		if ( said != T_Flag.f_unseen )
		{
			System.out.println( "sfv: saving new char's flag didn't give unseen" );
			return didnt;
		}
		return initial_assumption;
	}
	
	// consider confirming the state Trie is in when closing.
	public boolean repeat_first_var( Storage delta, String var, char n_f )
	{
		T_Flag report;
		IterableString iable = new IterableString( var );
		for ( Character ind : iable )
		{
			report = delta.determine_char( ind );
			if ( report != T_Flag.f_seen )
			{
				System.out.println( "rfv: seen char not giving seen" );
				return didnt;
			}
		}
		report = delta.check_flag( n_f );
		if ( report != T_Flag.f_seen )
		{
			System.out.println( "rfv: saving new char's flag didn't give unseen" );
			return didnt;
		}
		return initial_assumption;
	}

	public boolean repeat_key( Storage delta, String key, char n_f )
	{
		boolean worked = initial_assumption;
		T_Flag report;
		IterableString repeat = new IterableString( key );
		for ( Character ind : repeat )
		{
			report = delta.determine_char( ind );
			if ( report != T_Flag.f_seen )
			{
				System.out.println( "rk: key not giving seen: " + ind ); // tripping this. why?
				worked = didnt;
			}
			worked = check_state( delta, T_Flag.checking,
					"rk: matching key not in check mode" ) && worked;
		}
		report = delta.check_flag( n_f );
		if ( report != T_Flag.f_key )
		{
			System.out.println( "rk: ending key didn't produce diff flag" ); // also trips this
			worked = didnt;
		}
		return worked;
	}

	public boolean check_state( Storage delta, T_Flag expected, String error )
	{
		if ( ! state_matches( delta, expected ) )
		{
			System.out.println( error );
			return didnt;
		}
		return initial_assumption;
	}


	public char was_flag( T_Flag received )
	{
		if ( received == T_Flag.f_seen )
			return '@';
		if ( received == T_Flag.f_unseen )
			return '?';
		if ( received == T_Flag.f_key )
			return '*';
		else
			return 'X'; // assert unreachable
	}

	public void show_trie( Storage delta, boolean small_test_space )
	{
		if ( small_test_space )
			delta.show_tiny();
		else
			delta.reveal_thyself();
	}

	public boolean few_ids( int num_keys, int num_vars, int num_letters )
	{
		return ( num_keys + num_vars ) < num_letters ;
	}
}
