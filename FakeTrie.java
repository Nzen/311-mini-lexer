
public class FakeTrie extends Storage
{
	// serves as stub to test Lexer without a real FlatTrie
	// use a var with a $, just for total sake
	// > lexer needs fake constructor again
	boolean keymode;
	
	public FakeTrie( )
	{
		keymode = true; // for check flag()
	}

	@Override
	public T_Flag determine_char( char to_validate )
	{
		switch ( to_validate )
		{
		case '1' :
			return T_Flag.f_seen;
		case '2' :
			return T_Flag.f_unseen;
		case '3' :
			return T_Flag.f_key;
		default :
			return T_Flag.f_unseen;
		}
	}

	// initially get seen, then switch with key
	@Override
	public T_Flag check_flag( char a_flag )
	{
		keymode = ! keymode;
		return ( keymode ) ? T_Flag.f_key : T_Flag.f_seen;
	}

	// initially get initial, then switch to checking
	@Override
	public boolean t_matches_state( T_Flag should_be )
	{
		keymode = ! keymode;
		return should_be == (( keymode ) ? T_Flag.checking : T_Flag.initial);
	}

	@Override
	public boolean in_alphabet( char only_b )
	{
		return ( only_b == 'b' );
	}

	@Override
	public void reveal_thyself()
	{
		System.out.println( "I'm a fake trie" );
	}

	@Override
	public void show_tiny()
	{
		System.out.println( "I'm a fake trie, with little to show" );
	}
}
