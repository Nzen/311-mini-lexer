
import java.io.File;
import java.io.FileNotFoundException;
import java.lang.IllegalStateException;
import java.util.NoSuchElementException;
import java.util.Scanner;


/**
WordLoader: loads words or integers from a file.
This gets the filenames in the project 
*/
public class WordLoader
{
	private Scanner input;

	public void open( String fileName )
	{
		try
		{
			input = new Scanner( new File( fileName ) );
		}
		catch ( FileNotFoundException fileNotFoundException )
		{
			System.err.println( "Error opening file, not found." );
			System.exit( 1 ); // or try again
		}
	}
	
	public boolean moreLines( )
	{
		return input.hasNextLine();
	}

	public String read_line( )
	{
		try
		{
			return input.nextLine( );
		}
		catch ( NoSuchElementException elementException )
		{
			System.err.println( "File data improperly formed." );
			input.close();
			System.exit( 1 );
		}
		catch ( IllegalStateException stateException )
		{
			System.err.println( "Error reading from file." );
			System.exit( 1 );
		}
		return ""; // assert unreachable
	}

	public int read_int()
	{
		try
		{
			return input.nextInt();
		}
		catch ( NoSuchElementException elementException )
		{
			System.err.println( "File data improperly formed." );
			input.close();
			System.exit( 1 );
		}
		catch ( IllegalStateException stateException )
		{
			System.err.println( "Error reading from file." );
			System.exit( 1 );
		}
		return -9999; // assert unreachable
	}

	public void close( )
	{
	if ( input != null ) // ie, if it hasn't been closed
		input.close();
	}
}