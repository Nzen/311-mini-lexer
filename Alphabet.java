import java.util.HashMap;
import java.util.Set;

/**
Alphabet: reports presence and index for chars in provided string
*/
public class Alphabet
{
	HashMap<Character, Integer> symbols;

	public Alphabet( String sigma )
	{
		symbols = new HashMap<Character, Integer>( );
		int index = 0;
		IterableString all_together = new IterableString( sigma );
		for ( Character letter : all_together )
		{
			symbols.put( letter, index );
			index++;
		}
	}

	public boolean includes( Character unknown )
	{
		return symbols.containsKey( unknown );
	}

	public int stored_in( Character letter )
	{
		Integer where = symbols.get( letter ); 
		return ( where == null ) ? -1 : where ;
	}

	public int length()
	{
		return symbols.size();
	}

	String get_letters()
	{
		String nn = "";
		Set<Character> them = symbols.keySet();
		for ( Character within : them )
			nn += within;
		return nn;
	}
}
