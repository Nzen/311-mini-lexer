
public abstract class Storage
{
	// here so I can test Lexer with a Trie stub,
	// though it seems to have been premature functionality
	public abstract T_Flag determine_char( char to_validate );

	public abstract boolean in_alphabet( char bn );
	
	public abstract T_Flag check_flag( char a_flag );

	public abstract void reveal_thyself();

	public abstract boolean t_matches_state( T_Flag should_be );

	public abstract void show_tiny();
}