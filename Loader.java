import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.FileReader;

/**
Loader: a file reading class for single characters.
Buffered so it begins with your first character and tests validity when you ask for first. 
*/
public class Loader
{
	private FileReader input;
	private int[] buffer;
	private int newest_index;
	public final int eof = 65500;
	public final char EOF = (char)eof;
	
	public void open( String file_name )
	{
		try
		{
			input = new FileReader( file_name );
			buffer = new int[ 2 ];
			newest_index = 0;
			read_one();
		}
		catch ( FileNotFoundException fileNotFoundException )
		{
			System.err.printf( "Error opening %s, not found.%n", file_name );
			System.exit( 1 ); // or try again
		}
	}

	private void read_one()
	{
		try
		{
			buffer[ newest_index ] = input.read();
			switch_index();
		}
		catch (IOException woops )
		{
			System.err.println( "Y' read past the end of file." );
		}
	}

	public char read_next( )
	{
		// provide the client a character or EOF if not
		if ( has_more() )
		{
			read_one(); // switches the index as a side effect
			return ( char ) buffer[ newest_index ];
		}
		else
			return EOF;
	}

	public boolean has_more()
	{
		return buffer[ other_index() ] >= 0;// && buffer[ other_index() ] < eof ;
	}

	private void switch_index()
	{
		newest_index = ( newest_index == 0 ) ? 1 : 0;
	}

	private int other_index()
	{
		return ( newest_index == 0 ) ? 1 : 0;
	}

	public void close( )
	{
		try
		{
			input.close();
		}
		catch (IOException woops )
		{
			System.err.println( "I couldn't close the file. Execution hosed." );
		}
	}
}