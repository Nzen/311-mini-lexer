/**
Reads a list of keys, files. Flags those files with *?@
	Outputs saved state
*/

public class Project2
{
	static final int key_ind = 0;
	static final int input_ind = key_ind + 1;
	static final char nothing = '|';
	static final char get_several = '&';
	static final String fabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_$";

	public static void main( String[] args )
	{
		// real
		//String keys = args[0].substring( 0, args[0].length() - 1 );
		//run( keys, get_file_names( args[1] ) );
		//	tests
		run_tests();
		//run( "ShortKeys.txt", new String[] {"arg.java"} );
		//run( "MediumKeys.txt", new String[] {"Storage.java"} );
	}

	// first line in list_file is number of files
	static String[] get_file_names( String list_file )
	{
		WordLoader for_file_names = new WordLoader();
		for_file_names.open( list_file );
		int many = for_file_names.read_int();
		String[] modules = new String[ many ];
		int ind = 0;
		for_file_names.read_line(); // cut \n left by read_int
		while ( for_file_names.moreLines() )
		{
			modules[ ind ] = for_file_names.read_line();
			ind++;
		}
		for_file_names.close();
		return modules;
	}

	static void run( String reserved_file, String[] file_list )
	{
		Lexer java_lexer = new Lexer( fabet, nothing, get_several );
		save_key_words( reserved_file, java_lexer );
		for ( String file_f : file_list )
			flag_program( file_f, java_lexer );
		java_lexer.emit_saved_data();
	}

	static void save_key_words( String keyw_file, Lexer java_lexer )
	{
		Loader of_keys = new Loader();
		of_keys.open( keyw_file );
		while ( of_keys.has_more() )
		{
			java_lexer.enter_key( of_keys.read_next() );
		}
		// eof isn't sent for last key, so add this:
		java_lexer.enter_key( '\n' );
		of_keys.close();
		java_lexer.toggle_key_mode();
	}
	
	static void flag_program( String other_file, Lexer java_lexer )
	{
		char peek;
		char became;
		String flag_n_delim;
		Loader generic = new Loader();
		generic.open( other_file );
		while ( generic.has_more() )
		{
			peek = generic.read_next();
			became = java_lexer.lex_char( peek );
			if ( became == nothing )
				continue;
			else if ( became == get_several )
			{
				flag_n_delim = java_lexer.snag_buffer();
				if ( flag_n_delim.charAt( 1 ) == '\n' )
					System.out.printf( "%c%n", flag_n_delim.charAt( 0 ) );
				else
					System.out.print( flag_n_delim );
			}
			else if ( became == '\n' ) // for windows
				System.out.printf( "%n" );
			else
				System.out.print( became );
		}
		// eof isn't sent for last key, so add this:
		java_lexer.lex_char( '\n' );
		System.out.println( );
		generic.close();
	}

	static void run_tests()
	{
		TestSuite for_classes = new TestSuite();
		for_classes.test_iterable_string();
		for_classes.test_word_loader( "arg.java" );
		for_classes.test_loader( "arg.java" );
		for_classes.test_alphabet();
		for_classes.test_trie();
		for_classes.test_lexer();
		System.out.println( "-\nNeeds more tests" );
	}
}