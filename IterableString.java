import java.util.Iterator;

public class IterableString implements Iterable<Character>
{
	private String rigamarole;

	public IterableString( String to_use_the_for_each )
	{
		rigamarole = to_use_the_for_each; // wouldn't be so hard in python
	}

	public Iterator<Character> iterator( )
	{
		return new ConveyorBelt( rigamarole );
	}
	
	private class ConveyorBelt implements Iterator<Character>
	{
		final String java_limitation;
		int pointing_at = 0;
	
		public ConveyorBelt( String $to_iterate_over )
		{
			java_limitation = $to_iterate_over;
		}
	
		public boolean hasNext()
		{
			return pointing_at < java_limitation.length();
		}
	
		public Character next()
		{
			return java_limitation.charAt( pointing_at++ );
		}
	
		public void remove()
		{
			throw new UnsupportedOperationException();
		}
	}
}
