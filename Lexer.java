
/**
Lexer: determines the next character to print.
For keymode, this might be the char or the * flag
else, may be char, flag, a flag to grab two chars, or nothing
*/
public class Lexer
{
	private Storage transition_list;
	private L_Flag state;
	private T_Flag reported;
	private final char empty; // for garbage chars
	private final char multi; // when I know whether space or endl
	private boolean keymode; // for end flag selection
	private String buffer;

	public Lexer( String valid_start, char clients_epsilon, char signal_multi )
	{
		transition_list = new FlatTrie( valid_start, "*?" );
		empty = clients_epsilon;
		multi = signal_multi;
		state = L_Flag.new_line;
		buffer = "";
		keymode = true;
	}

	public void enter_key( char next )
	{
		if ( next == ' ' || next == '\n' ) // use *
			transition_list.check_flag( proper_flag( T_Flag.f_unseen ) );
		else if ( next == '\r' ) // windows ending
			return;
		else // id_text
			transition_list.determine_char( next );
	}

	public char lex_char( char next )
	{
		switch ( state )
		{
		case new_line :
			return check_line( next );
		case id_text :
			return check_id( next );
		case between :
			return check_between( next );
		default :
			return empty;
		}
	}

	// print nothing until valid start
	private char check_line( char next )
	{
		if ( starts_id( next ) )
		{
			state = L_Flag.id_text;
			reported = transition_list.determine_char( next );
			return next;
		}
		else
			return empty;
	}

	// wait for end, process id chars
	private char check_id( char next )
	{
		if ( is_newline( next ) )
			return ch_id_endl( next );
		else if ( ends_id( next ) )
			return ch_id_space( next );
		else
		{
			reported = transition_list.determine_char( next );
			return next;
		}
	}

	// emits flag + endl
	private char ch_id_endl( char next )
	{
		state = L_Flag.new_line;
		reported = transition_list.check_flag( '?' );
		buffer = Character.toString( proper_flag( reported ) ) + next;
		return multi;
	}

	// only emits flag, next might be nothing til endl
	private char ch_id_space( char next )
	{
		state = L_Flag.between;
		reported = transition_list.check_flag( '?' );
		return proper_flag( reported );
	}

	// emit _ + first ; endl ; or nothing
	private char check_between( char next )
	{
		if ( starts_id( next ) )
		{
			state = L_Flag.id_text;
			reported = transition_list.determine_char( next );
			buffer = " " + next;
			return multi;
		}
		else if ( is_newline( next ) )
		{
			state = L_Flag.new_line;
			return next;
		}
		else
		{
			return empty;
		}
	}

	private char proper_flag( T_Flag which )
	{
		if ( keymode || which == T_Flag.f_key )
			return '*';
		else if ( which == T_Flag.f_unseen )
			return '?';
		else
			return '@'; // flag.f_seen
	}
	
	public void toggle_key_mode( )
	{
		keymode = ! keymode;
	}
	
	// for client
	public String snag_buffer( )
	{
		return buffer;
	}

	private boolean starts_id( char dont_know )
	{
		return transition_list.in_alphabet( dont_know );
	}

	// neither a number nor in alphabet
	private boolean ends_id( char dont_know )
	{
		return ! ( is_number( dont_know ) || starts_id( dont_know ) );
	}

	private boolean is_number( char here )
	{
		return here >= '0' && here <= '9';
	}

	private boolean is_newline( char blugh )
	{
		return blugh == '\n';
	}

	public void emit_saved_data()
	{
		transition_list.reveal_thyself();
	}

	boolean t_matches_current_state( L_Flag from_out )
	{ // for testing
		return state == from_out;
	}

	public void emit_concise_data()
	{
		transition_list.show_tiny();
	}
}