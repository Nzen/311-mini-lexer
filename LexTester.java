
public class LexTester
{
	static final boolean good = true;
	static final boolean not = false;
	// current idea is constrained lang & input.
	//later try dependency problem with fake trie 

	public void prep_lex_keys( Lexer grammer_nazi )
	{
		IterableString key_1 = new IterableString( "axe " );
		IterableString key_2 = new IterableString( "bool " );
		// turns out that yak shaving may not have been totally useless
		for ( Character in_str : key_1 )
			grammer_nazi.enter_key( in_str );
		for ( Character in_str : key_2 )
			grammer_nazi.enter_key( in_str );
		grammer_nazi.toggle_key_mode();
	}

	public boolean t_lex_rej_bad( Lexer grammer_nazi, char nil )
	{
		char uu = grammer_nazi.lex_char( 'd' );
		if ( uu != nil )
		{
			System.out.println( "lex: accepted non sigma-" + uu );
			return not;
		}
		else if ( grammer_nazi.lex_char( '\n' ) != nil )
		{
			System.out.println( "lex: accepted preceeding endl" );
			return not;
		}
		else if ( grammer_nazi.lex_char( '8' ) != nil )
		{
			System.out.println( "lex: accepted number" );
			return not;
		}
		else
			return good;
	}

	public boolean t_l_first_var( Lexer grammer_nazi, String first_var )
	{
		boolean cc = true;
		IterableString var_1 = new IterableString( first_var );
		L_Flag id = L_Flag.id_text;
		L_Flag btwx = L_Flag.between;
		L_Flag[] st_should_pass = { id, id, id, btwx };
		char flag_;
		int nn = 0;
		// has ending, so state should be between after
		for ( Character let : var_1 )
		{
			flag_ = grammer_nazi.lex_char( let );
			if ( ! grammer_nazi.t_matches_current_state( st_should_pass[nn] ) )
			{
				System.out.println( "lex: didnt move to between state" );
				cc = not;
			}
			nn++;
		}
		return cc;
	}

	//	public void enter_key( char next )

	//public char lex_char( char next )

	public void t_l_many_vars( Lexer grammer_nazi, char more ) // just dumps in without testing
	{
		IterableString key_n_var = new IterableString( "axe l boo ace" + '\n' ); // woo fancy
		int linel = 5;
		for ( Character let : key_n_var )
		{
			if ( let == ' ' )
			{
				if ( linel % 5 == 4 )
				{
					System.out.println();
					linel = 0;
				}
				else
					linel--;
			}
			char now_is = grammer_nazi.lex_char( let );
			if ( now_is == more )
				System.out.print( grammer_nazi.snag_buffer() );
			else
				System.out.print( now_is );
		}
	}
}
