
public class TestSuite
{
	static final int key = 0;
	static final int var = 1;
	static final boolean hoped = true;
	static final boolean let_down = !hoped;

	public void test_alphabet()
	{
		System.out.println( " - Alphabet" );
		Alphabet english = new Alphabet( "etaolin" );
		if ( english.includes( '&' ) )
			System.out.println( "includes: false positive" );
		else if ( ! english.includes( 'e' ) )
			System.out.println( "includes: missed negative" );
		else if ( english.stored_in( 'e' ) != 0 )
			System.out.println( "stored_in false index" );
		else if ( english.stored_in( 'Q' ) >= 0 )
		{
			System.out.println( "stored_in gave index when got garbage" );
		}
		else
			System.out.println( " seems fine" );
	}

	public void test_loader( String bit_stream )
	{
		System.out.println( " - Loader (char-wise)" );
		String whole_file = "\r\npublic class arg\r\n" +
				"{\r\n\tpublic static void main( String[] args )\r\n" +
				"\t{\r\n\t\tfor ( String nn : args )\r\n" +
				"\t\t\tSystem.out.println( nn );\r\n" +
				"\t}\r\n}\r\n";
		boolean worked = hoped;
		Loader this_file = new Loader();
		this_file.open( bit_stream );
		char temp = 'f';
		int ind = 0;
		while ( this_file.has_more() )
		{
			temp = this_file.read_next();
			if ( temp != whole_file.charAt( ind ) )
			{
				System.out.printf( "expected %c @%d got %c%n", whole_file.charAt( ind ), ind, temp );
				worked = let_down;
			}
			ind++;
			//System.out.print( temp );
		}
		this_file.close();
		if ( worked )
			System.out.println( " didn't stumble" );
	}

	public void test_lexer( )
	{
		LexTester words = new LexTester ();
		System.out.println( " - Lexer" );
		char nil = '_';
		char more = '^';
		final String letras = "axecbol";
		boolean no_complaints = hoped;
		Lexer grammer_nazi = new Lexer( letras, nil, more );
		words.prep_lex_keys( grammer_nazi );
		no_complaints = words.t_lex_rej_bad( grammer_nazi, nil );
		

		// add words? test the stuff inside?
		
		String first_v = "ace ";
		no_complaints = words.t_l_first_var( grammer_nazi, first_v ) && no_complaints;

		//words.t_l_many_vars( grammer_nazi, more ); // just dumps in data, & prints. no no.

		if ( no_complaints )
			System.out.println( " no tests tripped" );
		//grammer_nazi.emit_concise_data(  );
	}

	public void test_trie( )
	{
		TrieTester ram = new TrieTester();
		String alpha = "etaolin";
		final char[] flags = { '*', '?' };
		Storage delta = new FlatTrie( alpha, new String( flags ) );
		String[] keys = { "ate" };
		String[] vars = { "tat" };
		boolean worked = hoped;
		System.out.println( " - Trie" );
		
		worked = ram.save_keys( delta, keys, flags[ key ] );
		worked = ram.save_first_var( delta, vars[ 0 ], flags[ var ] ) && worked;
		worked = ram.repeat_first_var( delta, vars[ 0 ], flags[ var ] ) && worked;
		worked = ram.repeat_key( delta, keys[ 0 ], flags[ var ] ) && worked;

		TrieTestWhiteBox cudgel = new TrieTestWhiteBox(alpha, flags); 
		int number_of_ids = 5;
		worked = cudgel.case_no_overlap( number_of_ids ) && worked;
		worked = cudgel.case_first_only_overlap( number_of_ids ) && worked;
		worked = cudgel.case_russian_dolls( number_of_ids ) && worked;
		//cudgel.show_trie();

		if ( worked )
			System.out.println( " didn't trip tests" );
		//ram.show_trie( delta, ram.few_ids( keys.length, vars.length, alpha.length() ) );
	}

	public void test_trie_visually() // human checked quine vs emergency bug
	{
		TrieTester eval = new TrieTester();
		IterableString f_key = new IterableString( "ni cut n nil cy ni nyf " );
		T_Flag says;
		char[] flags = { '*', '?' };
		String beta = "cutnlefbyred_i";
		FlatTrie compo = new FlatTrie( beta, new String(flags ) );
		for ( Character nu : f_key )
		{
			if ( nu == ' ' )
			{
				says = compo.check_flag( flags[ 1 ] );
				System.out.print( eval.was_flag( says ) + " " );
			}
			else
			{
				compo.determine_char( nu );
				System.out.print( nu );
			}
		}
		System.out.println( );
		compo.show_tiny();
	}

	public void test_iterable_string( )
	{
		String control = "into the grinder";
		System.out.println( " - Iterating string" );
		int where = 0;
		char should_be = ' ';
		boolean worked = hoped;
		IterableString loop = new IterableString( control );
		for ( Character pointer : loop )
		{
			should_be = control.charAt( where++ ); // idiomatic
			if ( pointer != should_be )
			{
				System.out.printf( "didn't match char %c : %c", pointer, should_be );
				worked = let_down;
			}
		}
		if ( worked )
			System.out.println( " seems fine" );
	}

	public void test_word_loader( String file_n )
	{
		boolean worked = hoped;
		String[] args_program = { "", "public class arg", "{",
					"\tpublic static void main( String[] args )",
					 "\t{", "\t\tfor ( String nn : args )",
					"\t\t\tSystem.out.println( nn );", "\t}", "}" };
		System.out.println( " - WordLoader" );
		WordLoader scanner = new WordLoader();
		String what_im_looking_at;
		int which = 0;
		scanner.open( file_n );
		while( scanner.moreLines() )
		{
			//System.out.println( scanner.read_line() );
			what_im_looking_at = scanner.read_line();
			if ( ! what_im_looking_at.equals( args_program[ which ] ) )
				{
					System.out.println( what_im_looking_at +
							" <is @" + Integer.toString( which ) + " want> "
							+ args_program[ which ] );
					worked = let_down;
				}
			which++;
		}
		scanner.close();
		if ( worked )
			System.out.println( " seems fine" );
	}
}



















