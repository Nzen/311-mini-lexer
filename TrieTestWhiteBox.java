
// as it says on the tin: this checks how flat_trie lays out its data
// it's a compromise between encapsulation and meeting the spec
public class TrieTestWhiteBox extends TrieTester
{
	private FlatTrie gamma;
	private String chars;
	private char[] flag;

	public TrieTestWhiteBox( String alpha, char[] ends)
	{
		chars = alpha;
		gamma = new FlatTrie( alpha, new String( ends ) );
		flag = new char[]{ ends[0],ends[1] };
	}

	public boolean case_no_overlap( int number_of_ids )
	{
		//gamma.t_empty_self(); currently first, otherwise uncomment
		String[] all_ids = make_non_overlapping_ids( number_of_ids );
		fill_trie( all_ids );
		return match_w_no_overlap( all_ids );
	}

	public boolean case_first_only_overlap( int number_of_ids )
	{
		gamma.t_empty_self();
		String[] all_ids = make_shared_first_ids( number_of_ids );
		fill_trie( all_ids );
		boolean sole_assumption = only_first_char_seen( );
		return match_w_first_only_overlap( all_ids ) && sole_assumption;
	}

	public boolean case_russian_dolls( int number_of_ids )
	{
		gamma.t_empty_self();
		String[] all_ids = mk_russian_doll_ids( number_of_ids );
		fill_trie( all_ids );
		boolean sole_assumption = only_first_char_seen( );
		//return match_russian_dolls( all_ids ) && sole_assumption;
		sole_assumption = match_russian_dolls( all_ids ) && sole_assumption;
		return sole_assumption;
	}

	private void fill_trie( String[] all_ids )
	{
		for( String one_id : all_ids )
		{
			for ( int ind = 0; ind < one_id.length() - 1; ind++ )
				gamma.determine_char( one_id.charAt( ind ) );
			gamma.check_flag( one_id.charAt( one_id.length() - 1 ) );
		}
	}

	private String[] make_non_overlapping_ids( int number_of_ids )
	{
		//alpha = "etaolin";
		//int lim = chars.length();
		String[] crate = new String[ number_of_ids ];
		int id_ind = 0;
		char key = '*';
		char var = '?';
		int start = 0;
		int offset = 3;
		int keys_to_use = 2;
		for ( int n_o_keys = keys_to_use; n_o_keys > 0; n_o_keys-- )
		{
			String a_key = chars.substring( start, start + offset ) + key;
			crate[ id_ind++ ] = a_key;
			start++;
		}
		for ( int n_o_vars = number_of_ids - keys_to_use; n_o_vars > 0; n_o_vars-- )
		{
			String a_var = chars.substring( start, start + offset ) + var;
			crate[ id_ind++ ] = a_var;
			start++;
		}
		return crate;
	}

	// very artificial situation. but good to build the format against.
	private boolean match_w_no_overlap( String[] everything )
	{
		boolean worked = initial_assumption;
		int should_be = 0;
		char focus;
		for( String id : everything )
		{
			int word_ind = 0;
			focus =  id.charAt( word_ind );
			if ( ! first_ch_matches( focus, should_be ) )
			{
				System.out.printf( "mw!o: first ch %c index %d didnt match%n", focus, should_be );
				worked = didnt;
			}
			word_ind++;
			//should_be++;
			for( ; word_ind < id.length(); word_ind++ )
			{
				focus =  id.charAt( word_ind );
				if ( ! later_ch_matches( id.charAt( word_ind ), should_be ) )
				{
					System.out.printf( "mw!o: later char %c didn't match index %d%n", focus, should_be );
					worked = didnt;
				}
				should_be++;
			}
		}
		final int unset = -1;
		for ( should_be--; should_be >= 0; should_be-- )
		{
			if ( skip_ind_matches( should_be, unset ) )
			{
				System.out.printf( "mw!o: skip at %d shouldnt have been set", should_be );
				worked = didnt;
			}
		}
		return worked;
	}

	private String[] make_shared_first_ids( int number_of_ids )
	{
		String[] crate = new String[ number_of_ids ];
		int id_ind = 0;
		final String key = "*";
		final String var = "?";
		int start = 0;
		int offset = 2;
		int keys_to_use = 2;
		final String first = "" + chars.charAt( start );
		for ( int n_o_keys = keys_to_use; n_o_keys > 0; n_o_keys-- )
		{
			String a_key = first + chars.substring( start, start + offset ) + key;
			crate[ id_ind++ ] = a_key;
			start++;
		}
		for ( int n_o_vars = number_of_ids - keys_to_use; n_o_vars > 0; n_o_vars-- )
		{
			String a_var = first + chars.substring( start, start + offset ) + var;
			crate[ id_ind++ ] = a_var;
			start++;
		}
		return crate;
	}

	private boolean only_first_char_seen( )
	{
		boolean worked = initial_assumption;
		final int first = 0;
		final int unseen = -1;
		char only_one = chars.charAt( first );
		IterableString rest = new IterableString( chars.substring( first + 1 ) );
		if ( ! gamma.t_initial_index( only_one, first ) )
		{
			System.out.printf( "ofcs: didnt match %d solitary used initial char%c%n", first, only_one );
			worked = didnt;
		}
		for ( Character each : rest )
		{
			if ( ! gamma.t_initial_index( each, unseen ) )
			{
				System.out.printf( "ofcs: other first char %c has been set%n", each );
				worked = didnt;
			}
		}
		return worked;
	}

	// all share the first character but are otherwise distinct
	private boolean match_w_first_only_overlap( String[] everything )
	{
		boolean worked = initial_assumption;
		int should_be = 0;
		final int skip_first = 1;
		int skip_position = should_be;
		// test first separately
		String first_id = everything[0];
		char shared = first_id.charAt( should_be );
		if ( ! first_ch_matches( shared, should_be ) )
		{
			System.out.printf( "mw1o: didnt match solitary first char %c%n", shared );
			worked = didnt;
		}
		should_be = mw1o_test_rest_of_id( skip_first, should_be, first_id );
		if ( should_be < 0 )
		{
			should_be *= -1;
			worked = didnt;
		}
		// rest of the ids
		for ( int times = skip_first; times < everything.length; times++ )
		{
			int word_ind = skip_first;
			if ( ! skip_ind_matches( skip_position, should_be ) )
			{
				System.out.printf( "mw1o: skip at %d didn't match expected, %d%n", skip_position, should_be );
				worked = didnt;
			}
			skip_position = should_be;
			should_be = mw1o_test_rest_of_id( skip_first, should_be, everything[times] );
			if ( should_be < 0 )
			{
				should_be *= -1;
				worked = didnt;
			}
		}
		return worked;
	}

	private int mw1o_test_rest_of_id( int skip_first, int should_be, String this_id )
	{
		boolean worked = initial_assumption;
		char other;
		for ( int rest_ch = skip_first; rest_ch < this_id.length(); rest_ch++ )
		{
			other = this_id.charAt( rest_ch );
			if ( ! later_ch_matches( other, should_be ) )
			{
				System.out.printf( "mw1o: later -%c- didnt match at %d%n", other, should_be );
				worked = didnt;
			}
			should_be++;
		}
		return (worked) ? should_be : should_be * -1;
	}

	private String[] mk_russian_doll_ids( int number_of_ids )
	{
		//alpha = "etaolin";
		String[] crate = new String[ number_of_ids ];
		int id_ind = 0;
		String key = "*";
		String var = "?";
		int lim = chars.length();
		int keys_to_use = 2;
		int offset = 0;
		int start = 0;
		for ( int n_o_keys = keys_to_use; n_o_keys > 0; n_o_keys-- )
		{
			String a_key = chars.substring( start, lim - offset ) + key;
			crate[ id_ind++ ] = a_key;
			offset++;
		}
		for ( int n_o_vars = number_of_ids - keys_to_use; n_o_vars > 0; n_o_vars-- )
		{
			String a_key = chars.substring( start, lim - offset ) + var;
			crate[ id_ind++ ] = a_key;
			offset++;
		}
		return crate;
	}

	// first id is longest, rest are decreasing size substrings
	private boolean match_russian_dolls( String[] everything )
	{
		/*
		test it as lexer ie see keys for the first two strings and seen for the other three
		test that the letters were saved from the first, and then **???
		test skiplist, ie from len of every[1] - [4], that the skips are increased by 1
		 */
		//alpha = "etaolin";
		boolean worked = initial_assumption;
		char key = '*';
		char var = '?';
		int longest = 0;
		int word_len = everything[ longest ].length() - 1;
		int should_be = 0;
		IterableString only_word = new IterableString( everything[ longest ] );
		for ( Character ln : only_word )
		{
			if ( ln == 'e' )
			{
				if ( ! first_ch_matches( ln, should_be ) )
					{
						System.out.printf( "mRd: didn't match first key %c at %d%n",
								everything[ longest ].charAt( should_be ), should_be );
						worked = didnt;
					}
				should_be--; // to fix the index reset between gamma & rest_str
			}
			else
			{
				if ( ! later_ch_matches( ln, should_be ) )
				{
					System.out.printf( "mRd: didn't match later key %c at %d%n", ln, should_be );
					worked = didnt;
				}
			}
			should_be++;
		}// test remaining in rest: just flags
		word_len--;
		for ( int rest = 1; rest < everything.length; rest++ )
		{
			if ( ! later_ch_matches( everything[ rest ].charAt( word_len ), should_be ) )
			{
				System.out.printf( "mRd: subsequent flag %c at %d didnt match expectation%n",
						everything[ longest ].charAt( should_be ), should_be );
				worked = didnt;
			}
			should_be++;
			word_len--;
		}
		// now test the skiplist (though it clearly worked if above passes)
		int from_skip = everything[ longest ].length() + word_len;
		for ( int in_skip = word_len; in_skip < everything.length; in_skip++ )
		{
			if ( ! skip_ind_matches( in_skip, from_skip ) )
			{
				System.out.printf( "mRd: skip index %d != %d, expected%n", in_skip, word_len - 1 );
				worked = didnt;
			}
			from_skip--;
		}
		return worked;
	}

	// expects a a char,index pair to match on first_chars value, or -1 if not used
	private boolean first_ch_matches( char symb, int ind )
	{
		return gamma.t_initial_index( symb, ind );
	}

	private boolean later_ch_matches( char symb, int ind )
	{
		return gamma.t_char_in_rest_at( symb, ind );
	}

	private boolean skip_ind_matches( int at, int val )
	{
		return gamma.t_ind_in_skip_list( at, val );
	}

	public void show_trie()
	{
		// tests are small, so just
		gamma.show_tiny();
	}
}
